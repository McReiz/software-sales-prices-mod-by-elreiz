<?php
if (!defined('ABSPATH')) exit;

?>
<script type="text/javascript">

  var config = {  
    requiredText : "<?php echo __('This field is required.', 'product-options-for-woocommerce'); ?>",
    productId : <?php echo (int) $this->getProductId(); ?>,    
    productPrice : <?php echo (float) $this->getProductPrice(); ?>,
    numberOfDecimals : <?php echo (int) $this->getNumberOfDecimals(); ?>,    
    decimalSeparator : "<?php echo $this->getDecimalSeparator(); ?>",
    thousandSeparator : "<?php echo $this->getThousandSeparator(); ?>",
    currencyPosition : "<?php echo $this->getCurrencyPosition(); ?>",
    isOnSale : <?php echo (int) $this->getIsOnSale(); ?>       
  };
  
  var optionData = <?php echo $this->getOptionDataJson(); ?>;
   
  jQuery.extend(config, optionData);
    
  jQuery('#pwzrt_product_options').pwzrtProductOptions(config);    

</script>