<?php
if (!defined('ABSPATH')) exit;

class Thakur_ProductOptions_Controller_Product {

	private $_block;

	public function __construct() {
    add_action('woocommerce_before_add_to_cart_button', array($this, 'display_options_on_product_page'));
    add_action('wp_footer', array($this, 'script_options_to_footer'));
	}

	public function getBlock(){
		include_once(Thakur_PO()->getPluginPath() . 'Block/Product/Options.php');
		$this->_block = new Thakur_ProductOptions_Block_Product_Options();

		return $this->_block;
	}

	public function display_options_on_product_page() { 
		if( count($this->getBlock()->getOptions() ) > 0 ){
			echo $this->getBlock()->toHtml();
		}
	}

	public function script_options_to_footer(){
		if(!is_product()) return false;

		if( count($this->getBlock()->getOptions() ) > 0 ){
			echo $this->getBlock()->toScript();
		}
	}
}
